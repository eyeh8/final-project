CXX = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CXXFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

.PHONY: all
all:  checkersrunner

checkersrunner.o: checkersrunner.cpp checkers.hpp
	$(CXX) -c checkersrunner.cpp $(CXXFLAGS)



checkersrunner: checkersrunner.o
	$(CXX) -o checkersrunner checkersrunner.o $(CXXFLAGS)


clean:
	rm -f *.o checkersrunner


