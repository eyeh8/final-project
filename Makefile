CXX = g++
CONSERVATIVE_FLAGS = -std=c++11 -Wall -Wextra -pedantic
DEBUGGING_FLAGS = -g -O0
CXXFLAGS = $(CONSERVATIVE_FLAGS) $(DEBUGGING_FLAGS)

.PHONY: all
all: games test_games

games: games.o
	$(CXX) -o games games.o $(CXXFLAGS)

games.o: battleship.hpp checkers.hpp game.hpp
	$(CXX) -c games.cpp $(CXXFLAGS)

<<<<<<< HEAD
play_bs.o: play_bs.cpp battleship.hpp checkers.hpp game.hpp
	$(CXX) -c play_bs.cpp $(CXXFLAGS)
=======
test_games.o: battleship.hpp game.hpp
	$(CXX) -c test_games.cpp $(CXXFLAGS)
>>>>>>> 73564fcbd87e112df3f8a8092174949789c40d7f

test_games: test_games.o
	$(CXX) -o test_games test_games.o $(CXXFLAGS)

clean:
	rm -f *.o  test_games test_games.o games.o games
