#include "battleship.hpp"
#include <iostream>
#include <string>
#include <array>

using namespace std;

int main()
{
    cout << "CHOOSE A GAME:";
    int gameOption;
    string playerInput;
    cin >> gameOption;
    if(gameOption == 1)
    {
        Board player1_ships(10, 10);
        Board player2_ships(10, 10);
        BattleshipGame bsg;
// placing the ships on the boards
        player1_ships = bsg.placeAllShips(player1_ships, 1);
        if(bsg.checkQuit())
            return 0;
        player2_ships = bsg.placeAllShips(player2_ships, 2);
        if(bsg.checkQuit())
            return 0;
        cout << player1_ships << endl;
        cout << player2_ships << endl;
        while(1)
        {
            if(bsg.player1Dead())
            {
                cout << "PLAYER 2 WON" << endl;
                return 0;
            }
            if(bsg.player2Dead())
            {
                cout << "PLAYER 1 WON" << endl;
                return 0;
            }
            bsg.changeMoveSuccess(false);
            while(!bsg.getMoveSuccess())
            {
                player2_ships = bsg.playGame(player2_ships, 1);
                if(bsg.checkQuit())
                    return 0;
                cout << player1_ships << endl;
            }
            bsg.changeMoveSuccess(false);
            while(!bsg.getMoveSuccess())
            {
                player1_ships = bsg.playGame(player1_ships, 2);
                if(bsg.checkQuit())
                    return 0;
                cout << player2_ships << endl;
            }
        }
    }
    if(gameOption == 2)
    {
        cout << "mobile battleship" << endl;
    }
    if(gameOption == 3)
    {
        cout << "checkers" << endl;
    }
    return 0;
}
