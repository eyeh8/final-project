#ifndef CHECKERS_HPP_
#define CHECKERS_HPP_

#include <array>
#include <iostream>
#include <utility>
typedef std::pair<int, int> Coord;
class checkers{
public:
	void fillboard(){
		//row collumb top pieces
		board[0][1]=1;
		board[0][3]=1;
		board[0][5]=1;
		board[0][7]=1;

		board[1][0]=1;
		board[1][2]=1;
		board[1][4]=1;
		board[1][6]=1;

		board[2][1]=1;
        board[2][3]=1;
        board[2][5]=1;
        board[2][7]=1;


		//bottom pieces
        board[5][0]=-1;
        board[5][2]=-1;
        board[5][4]=-1;
        board[5][6]=-1;

        board[6][1]=-1;
        board[6][3]=-1;
        board[6][5]=-1;
        board[6][7]=-1;

		board[7][0]=-1;
        board[7][2]=-1;
        board[7][4]=-1;
        board[7][6]=-1;
	}
	int select_piece(int row, int collumb){
	//	((turn==1) ? board2 : board1)[row][collumb];
		current_piece_cord= std::make_pair(-1,-1);
		if(  (turn==1) && (board[row][collumb]==1 || board[row][collumb]==2) ){
			current_piece_cord=std::make_pair(row,collumb);
			return 1;
		} 
		if( (turn==-1) && (board[row][collumb]==-1 || board[row][collumb]==-2) ){
           // std::cout<<"here is the error"<<std::endl;
			current_piece_cord=std::make_pair(row,collumb);
			return -1;
        }
		return 0;
	}
	int check_jumps(int row, int collumb){
		int i=row;
		int j=collumb;
				if( j>1 && j<6 && i>1 && i<6 ){
                    // checks non edge cases for stuff
                    if( (turn==1 && board[i][j]==1) ){
						if( (board[i-1][j+1]==-1 && board[i-2][j+2]==0) ||(board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i-1][j+1]==-2&& board[i-2][j+2]==0) 
						||(board[i+1][j+1]==-2&& board[i+2][j+2]==0))
                            return 1;
					}
					if( (turn==1 && board[i][j]==2) ){
                        if((board[i-1][j+1]==-1&& board[i-2][j+2]==0) ||(board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i-1][j+1]==-2&& board[i-2][j+2]==0)
 						||(board[i+1][j+1]==-2&& board[i+2][j+2]==0) || (board[i-1][j-1]==-1&& board[i-2][j-2]==0) ||(board[i+1][j-1]==-1&& board[i+2][j-2]==0) 
						||(board[i-1][j-1]==-2&& board[i-2][j-2]==0) ||(board[i+1][j-1]==-2&& board[i+2][j-2]==0))
                            return 1;
                    }
                    if( (turn==-1 && board[i][j]==-1) ){
                        if((board[i-1][j-1]==1&& board[i-2][j-2]==0) ||(board[i+1][j-1]==1&& board[i+2][j-2]==0) ||(board[i-1][j-1]==2&& board[i-2][j-2]==0) 
						||(board[i+1][j-1]==2&& board[i+2][j-2]==0))
                            return -1;
                    }
					if( (turn==-1 && board[i][j]==-2) ){
                        if((board[i-1][j+1]==1&& board[i-2][j+2]==0) ||(board[i+1][j+1]==1&& board[i+2][j+2]==0) ||(board[i-1][j+1]==2&& board[i-2][j+2]==0)
                        ||(board[i+1][j+1]==2&& board[i+2][j+2]==0) || (board[i-1][j-1]==1&& board[i-2][j-2]==0) ||(board[i+1][j-1]==1&& board[i+2][j-2]==0) 
                        ||(board[i-1][j-1]==2&& board[i-2][j-2]==0) ||(board[i+1][j-1]==2&& board[i+2][j-2]==0))
                            return 1;
                    }
                }
// if block break
//combine edge casese
// case 2 left edge2 excluding kings on top2 or bottom2/////////////copy point
                // case normal piece from bottom
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==-1) && i!=0){
                    if((board[i-1][j+1]==1&& board[i-2][j+2]==0) ||(board[i-1][j+1]==2&& board[i-2][j+2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)
                        return -1;
                }//case king from bottom
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==-2) && i<6 && i>1 ){
                    if( (board[i+1][j+1]==1&& board[i+2][j+2]==0) ||(board[i+1][j+1]==2&& board[i+2][j+2]==0) ||(board[i-1][j+1]==1&& board[i-2][j+2]==0) 
					||(board[i-1][j+1]==2&& board[i-2][j+2]==0))
                        return -1;
                }//case normal piece from top
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==1) && i!=7){
                    if((board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i+1][j+1]==-2&& board[i+2][j+2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)
                        return 1;
                }//case king from top
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==2) && i<6 && i>1 ){
                    if( (board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i+1][j+1]==-2&& board[i+2][j+2]==0) ||(board[i-1][j+1]==-1&& board[i-2][j+2]==0) 
					||(board[i-1][j+1]==-2&& board[i-2][j+2]==0))
                        return 1;
                }
// case 3 right edge same drawbacks
				if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==-1) && i!=0){
                    if((board[i-1][j-1]==1&& board[i-2][j-2]==0) ||(board[i-1][j-1]==2&& board[i-2][j-2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)//copy point
                        return -1;
                }//case king from bottom
                if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==-2) && i<6 && i>1 ){
                    if( (board[i+1][j-1]==1&& board[i+2][j-2]==0) ||(board[i+1][j-1]==2&& board[i+2][j-2]==0) ||(board[i-1][j-1]==1&& board[i-2][j-2]==0) 
                    ||(board[i-1][j-1]==2&& board[i-2][j-2]==0))
                        return -1;
                }//case normal piece from top
                if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==1) && i!=7){
                    if((board[i+1][j-1]==-1&& board[i+2][j-2]==0) ||(board[i+1][j-1]==-2&& board[i+2][j-2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)
                        return 1;
                }//case king from top
                if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==2) && i<6 && i>1 ){
                    if( (board[i+1][j-1]==-1&& board[i+2][j-2]==0) ||(board[i+1][j-1]==-2&& board[i+2][j-2]==0) ||(board[i-1][j-1]==-1&& board[i-2][j-2]==0) 
                    ||(board[i-1][j-1]==-2&& board[i-2][j-2]==0))
                        return 1;
                }
// case 4 for kings in corners
                // top left corner king from bottom
                if( (i==0||i==1) && (j==0||j==1) && board[i][j]==-2){
                    if( (board[i+1][j+1]==1&& board[i+2][j+2]==0) || (board[i+1][j+1]==2&& board[i+2][j+2]==0))
                        return -1;
                }// top left corner king from top///////////////////copy point
                if( (i==0||i==1) && (j==0||j==1) && board[i][j]==2){
                    if( (board[i+1][j+1]==-1&& board[i+2][j+2]==0) || (board[i+1][j+1]==-2&& board[i+2][j+2]==0))
                        return 1;
                }// top right corner king from bottom
                if( (i==0||i==1) && (j==7||j==6) && board[i][j]==-2){
                    if( (board[i-1][j+1]==1&& board[i-2][j+2]==0) || (board[i-1][j+1]==2&& board[i-2][j+2]==0))
                        return -1;
                }// top right corner king from top
                if( (i==0||i==1) && (j==7||j==6) && board[i][j]==2){
                    if( (board[i-1][j+1]==-1&& board[i-2][j+2]==0) || (board[i-1][j+1]==-2&& board[i-2][j+2]==0))
                        return 1;
                }// bottom left corner king from bottom
                if( (i==7||i==6) && (j==0||j==1) && board[i][j]==-2){
                    if( (board[i+1][j-1]==1&& board[i+2][j-2]==0) || (board[i+1][j-1]==2&& board[i+2][j-2]==0))
                        return -1;
                }// bottom left corner king from top
                if( (i==7||i==6) && (j==0||j==1) && board[i][j]==2){
                    if( (board[i+1][j-1]==-1&& board[i+2][j-2]==0) || (board[i+1][j-1]==-2&& board[i+2][j-2]==0))
                        return 1;
                }// bottom right corner king from bottom
                if( (i==7||i==6) && (j==7||j==6) && board[i][j]==-2){
                    if( (board[i-1][j-1]==1&& board[i-2][j-2]==0) || (board[i-1][j-1]==2&& board[i-2][j-2]==0))//////////////copy point
                        return -1;
                }// bottom left corner king from top
                if( (i==7||i==6) && (j==7||j==6) && board[i][j]==2){
                    if( (board[i-1][j-1]==-1&& board[i-2][j-2]==0) || (board[i-1][j-1]==-2&& board[i-2][j-2]==0))
                        return 1;
                }
// case 5 kings on top2 middle edge
                // top king on top edge
                if( (i==0||i==1) && j>1 && j<6 && board[i][j]==2){
                    if( (board[i+1][j-1]==-1&& board[i+2][j-2]==0) || (board[i+1][j-1]==-2&& board[i+2][j-2]==0) || (board[i+1][j+1]==-1&& board[i+2][j+2]==0)
					 || (board[i+1][j+1]==-2&& board[i+2][j+2]==0))
                        return 1;
                }// bottom king on top edge
                if( (i==0||i==1) && j>1 && j<6 && board[i][j]==-2){
                    if( (board[i+1][j-1]==1&& board[i+2][j-2]==0) || (board[i+1][j-1]==2&& board[i+2][j-2]==0) || (board[i+1][j+1]==1&& board[i+2][j+2]==0) 
					|| (board[i+1][j+1]==2&& board[i+2][j+2]==0))
                        return -1;
                }
// case 6 kings on bottom middle edge
                // top king on bottom edge
                if( (i==7||i==6) && j>1 && j<6 && board[i][j]==2){
                    if( (board[i-1][j-1]==-1&& board[i-2][j-2]==0) || (board[i-1][j-1]==-2&& board[i-2][j-2]==0) || (board[i-1][j+1]==-1&& board[i-2][j+2]==0)//copy
					|| (board[i-1][j+1]==-2&& board[i-2][j+2]==0))
                        return 1;
                }// bottom king on bottom edge
                if( (i==7||i==6) && j>1 && j<6 && board[i][j]==-2){
                    if( (board[i-1][j-1]==1&& board[i-2][j-2]==0) || (board[i-1][j-1]==2&& board[i-2][j-2]==0) || (board[i-1][j+1]==1&& board[i-2][j+2]==0) 
					|| (board[i-1][j+1]==2&& board[i-2][j+2]==0))
                        return -1;
                }
		return 0;
	}
	void check_jumps(){
// tourent statement opperator issues with checking weather there is something in there edge cases like if your shit is on the side
		// note i=row j=collumb this may be wrong and need to be adjusted at some point
		for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                if( j>1 && j<6 && i>1 && i<6 ){
                    // checks non edge cases for stuff
                    if( (turn==1 && board[i][j]==1) ){
                        if( (board[i-1][j+1]==-1 && board[i-2][j+2]==0) ||(board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i-1][j+1]==-2&& board[i-2][j+2]==0) 
                        ||(board[i+1][j+1]==-2&& board[i+2][j+2]==0))
                            p1_jumps=1;
                    }
                    if( (turn==1 && board[i][j]==2) ){
                        if((board[i-1][j+1]==-1&& board[i-2][j+2]==0) ||(board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i-1][j+1]==-2&& board[i-2][j+2]==0)
                        ||(board[i+1][j+1]==-2&& board[i+2][j+2]==0) || (board[i-1][j-1]==-1&& board[i-2][j-2]==0) ||(board[i+1][j-1]==-1&& board[i+2][j-2]==0) 
                        ||(board[i-1][j-1]==-2&& board[i-2][j-2]==0) ||(board[i+1][j-1]==-2&& board[i+2][j-2]==0))
                            p1_jumps=1;
                    }
                    if( (turn==-1 && board[i][j]==-1) ){
                        if((board[i-1][j-1]==1&& board[i-2][j-2]==0) ||(board[i+1][j-1]==1&& board[i+2][j-2]==0) ||(board[i-1][j-1]==2&& board[i-2][j-2]==0) 
                        ||(board[i+1][j-1]==2&& board[i+2][j-2]==0))
                            p2_jumps=1;
                    }
                    if( (turn==-1 && board[i][j]==-2) ){
                        if((board[i-1][j+1]==1&& board[i-2][j+2]==0) ||(board[i+1][j+1]==1&& board[i+2][j+2]==0) ||(board[i-1][j+1]==2&& board[i-2][j+2]==0)
                        ||(board[i+1][j+1]==2&& board[i+2][j+2]==0) || (board[i-1][j-1]==1&& board[i-2][j-2]==0) ||(board[i+1][j-1]==1&& board[i+2][j-2]==0) 
                        ||(board[i-1][j-1]==2&& board[i-2][j-2]==0) ||(board[i+1][j-1]==2&& board[i+2][j-2]==0))
                            p1_jumps=1;
                    }
                }
// if block break
//combine edge casese
// case 2 left edge2 excluding kings on top2 or bottom2
                // case normal piece from bottom
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==-1) && i!=0){
                    if((board[i-1][j+1]==1&& board[i-2][j+2]==0) ||(board[i-1][j+1]==2&& board[i-2][j+2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)
                        p2_jumps=1;
                }//case king from bottom
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==-2) && i<6 && i>1 ){
                    if( (board[i+1][j+1]==1&& board[i+2][j+2]==0) ||(board[i+1][j+1]==2&& board[i+2][j+2]==0) ||(board[i-1][j+1]==1&& board[i-2][j+2]==0) 
                    ||(board[i-1][j+1]==2&& board[i-2][j+2]==0))
                        p2_jumps=1;
                }//case normal piece from top
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==1) && i!=7){
                    if((board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i+1][j+1]==-2&& board[i+2][j+2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)
                        p1_jumps=1;
                }//case king from top
                if( ((j==0)||j==1) && (/*board[i][j]==-2 ||*/ board[i][j]==2) && i<6 && i>1 ){
                    if( (board[i+1][j+1]==-1&& board[i+2][j+2]==0) ||(board[i+1][j+1]==-2&& board[i+2][j+2]==0) ||(board[i-1][j+1]==-1&& board[i-2][j+2]==0) 
                    ||(board[i-1][j+1]==-2&& board[i-2][j+2]==0))
                        p1_jumps=1;
                }
// case 3 right edge same drawbacks
                if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==-1) && i!=0){
                    if((board[i-1][j-1]==1&& board[i-2][j-2]==0) ||(board[i-1][j-1]==2&& board[i-2][j-2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)
                        p2_jumps=1;
                }//case king from bottom
                if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==-2) && i<6 && i>1 ){
                    if( (board[i+1][j-1]==1&& board[i+2][j-2]==0) ||(board[i+1][j-1]==2&& board[i+2][j-2]==0) ||(board[i-1][j-1]==1&& board[i-2][j-2]==0) 
                    ||(board[i-1][j-1]==2&& board[i-2][j-2]==0))
                        p2_jumps=1;
                }//case normal piece from top
                if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==1) && i!=7){
                    if((board[i+1][j-1]==-1&& board[i+2][j-2]==0) ||(board[i+1][j-1]==-2&& board[i+2][j-2]==0) /*|| board[i+1][j+1]==-1 || board[i+1][j+1]==-2*/)
                        p1_jumps=1;
                }//case king from top
                if( ((j==7)||j==6) && (/*board[i][j]==-2 ||*/ board[i][j]==2) && i<6 && i>1 ){
                    if( (board[i+1][j-1]==-1&& board[i+2][j-2]==0) ||(board[i+1][j-1]==-2&& board[i+2][j-2]==0) ||(board[i-1][j-1]==-1&& board[i-2][j-2]==0) 
                    ||(board[i-1][j-1]==-2&& board[i-2][j-2]==0))
                        p1_jumps=1;
                }
// case 4 for kings in corners
                // top left corner king from bottom
                if( (i==0||i==1) && (j==0||j==1) && board[i][j]==-2){
                    if( (board[i+1][j+1]==1&& board[i+2][j+2]==0) || (board[i+1][j+1]==2&& board[i+2][j+2]==0))
                        p2_jumps=1;
                }// top left corner king from top
                if( (i==0||i==1) && (j==0||j==1) && board[i][j]==2){
                    if( (board[i+1][j+1]==-1&& board[i+2][j+2]==0) || (board[i+1][j+1]==-2&& board[i+2][j+2]==0))
                        p1_jumps=1;
                }// top right corner king from bottom
                if( (i==0||i==1) && (j==7||j==6) && board[i][j]==-2){
                    if( (board[i-1][j+1]==1&& board[i-2][j+2]==0) || (board[i-1][j+1]==2&& board[i-2][j+2]==0))
                        p2_jumps=1;
                }// top right corner king from top
                if( (i==0||i==1) && (j==7||j==6) && board[i][j]==2){
                    if( (board[i-1][j+1]==-1&& board[i-2][j+2]==0) || (board[i-1][j+1]==-2&& board[i-2][j+2]==0))
                        p1_jumps=1;
                }// bottom left corner king from bottom
                if( (i==7||i==6) && (j==0||j==1) && board[i][j]==-2){
                    if( (board[i+1][j-1]==1&& board[i+2][j-2]==0) || (board[i+1][j-1]==2&& board[i+2][j-2]==0))
                        p2_jumps=1;
                }// bottom left corner king from top
                if( (i==7||i==6) && (j==0||j==1) && board[i][j]==2){
                    if( (board[i+1][j-1]==-1&& board[i+2][j-2]==0) || (board[i+1][j-1]==-2&& board[i+2][j-2]==0))
                        p1_jumps=1;
                }// bottom right corner king from bottom
                if( (i==7||i==6) && (j==7||j==6) && board[i][j]==-2){
                    if( (board[i-1][j-1]==1&& board[i-2][j-2]==0) || (board[i-1][j-1]==2&& board[i-2][j-2]==0))
                        p2_jumps=1;
                }// bottom left corner king from top
                if( (i==7||i==6) && (j==7||j==6) && board[i][j]==2){
                    if( (board[i-1][j-1]==-1&& board[i-2][j-2]==0) || (board[i-1][j-1]==-2&& board[i-2][j-2]==0))
                        p1_jumps=1;
                }
// case 5 kings on top2 middle edge
                // top king on top edge
                if( (i==0||i==1) && j>1 && j<6 && board[i][j]==2){
                    if( (board[i+1][j-1]==-1&& board[i+2][j-2]==0) || (board[i+1][j-1]==-2&& board[i+2][j-2]==0) || (board[i+1][j+1]==-1&& board[i+2][j+2]==0)
                     || (board[i+1][j+1]==-2&& board[i+2][j+2]==0))
                        p1_jumps=1;
                }// bottom king on top edge
                if( (i==0||i==1) && j>1 && j<6 && board[i][j]==-2){
                    if( (board[i+1][j-1]==1&& board[i+2][j-2]==0) || (board[i+1][j-1]==2&& board[i+2][j-2]==0) || (board[i+1][j+1]==1&& board[i+2][j+2]==0) 
                    || (board[i+1][j+1]==2&& board[i+2][j+2]==0))
                        p2_jumps=1;
                }
// case 6 kings on bottom middle edge
                // top king on bottom edge
                if( (i==7||i==6) && j>1 && j<6 && board[i][j]==2){
                    if( (board[i-1][j-1]==-1&& board[i-2][j-2]==0) || (board[i-1][j-1]==-2&& board[i-2][j-2]==0) || (board[i-1][j+1]==-1&& board[i-2][j+2]==0) 
                    || (board[i-1][j+1]==-2&& board[i-2][j+2]==0))
                        p1_jumps=1;
                }// bottom king on bottom edge
                if( (i==7||i==6) && j>1 && j<6 && board[i][j]==-2){
                    if( (board[i-1][j-1]==1&& board[i-2][j-2]==0) || (board[i-1][j-1]==2&& board[i-2][j-2]==0) || (board[i-1][j+1]==1&& board[i-2][j+2]==0) 
                    || (board[i-1][j+1]==2&& board[i-2][j+2]==0))
                        p2_jumps=1;
                }
			//forloop parens
			}
        }
//          for loop break finally
	}
	void make_king(){
		//checks the top and bottom rows for regular pieces in need of being made king
		for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
				if( (i==0) /*&& turn==-1*/ && ( board[i][j]==-1) )
					board[i][j]=-2;
				if( (i==7) /*&& turn==-1*/ && ( board[i][j]==1) )
					board[i][j]=2;
			}
		}
	}
	int check_king(){
		return board[current_piece_cord.first][current_piece_cord.second];
	}
	int p1_attack(int row, int collumb, char one, char two){
		//for p1 normal piece
		select_piece(row,collumb);
		if(current_piece_cord.first==-1)
			return -5;
		if(check_king()==1){
			// checkers can't move backwards
			if(one=='t')
				return -5;
			if(one=='b'){
				// case 1 trying to move down when allready at the bottom of the map
				if(row==7)
					return -4;
				// case 2 trying to move down and left
				if(two=='l'){
					// s^1 case 1 desired move left is off the map
					if(collumb==0)
						return -3;
					// s^1 case 2 there is nothing in desired move location and it can just move there
					if(board[row+1][collumb-1]==0){
						board[row+1][collumb-1]=1;
						board[row][collumb]=0;
						return 1;
					}
					// s^1 case 2.5 there is a friendly in the desired location so you can't jump
					if(board[row+1][collumb-1]==1 || board[row+1][collumb-1]==2){
						return -6;
					}
					// s^1 case 3  there is an enemy in the desired location
					if(board[row+1][collumb-1]==-1 ||board[row+1][collumb-1]==-2){ 
						// s^2 case 1 the hop location is off the board
						if( (row+2)>=7 || (collumb-2)<=0)
							return -4;
						//s^2 case 2 the space behind the piece is empty and hops there
						if(board[row+2][collumb-2]==0){
							board[row+1][collumb-1]=0;
							board[row+2][collumb-2]=1;
							board[row][collumb]=0;
							std::cout<<"JUMPED"<<std::endl;
							return 2;
						}
						//s^2 case 3 there is something in the hop desitnation so it can't hop or move
						if(board[row+2][collumb-2]!=-1 && board[row+2][collumb-2]!=-2 && board[row+2][collumb-2]!=1 && board[row+2][collumb-2]!=2)
							return -5;
					}// copy point ////////////////////////////////////////////////////////////////////////////////////////////////////
				}// case 2 moves down and right or at least wants to

				if(two=='r'){
                    // s^1 case 1 desired move right is off the map
                    if(collumb==7)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row+1][collumb+1]==0){
                        board[row+1][collumb+1]=1;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row+1][collumb+1]==1 || board[row+1][collumb+1]==2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row+1][collumb+1]==-1 ||board[row+1][collumb+1]==-2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row+2)>=7 || (collumb+2)>=7)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there  ///////////////////copy point///////////////////////
                        if(board[row+2][collumb+2]==0){
                            board[row+2][collumb+2]=1;
                            board[row][collumb]=0;
							board[row+1][collumb+1]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row+2][collumb+2]!=-1 && board[row+2][collumb+2]!=-2 && board[row+2][collumb+2]!=1 && board[row+2][collumb+2]!=2)
                            return -5;
                    }
				}
			}
		}







		//for p1 king
		if(check_king()==2){
            if(one=='t'){///////////////////copy point//////////////////////////////////////
                // case 1 trying to move up when allready at the top of the map
				if(row==0)
                    return -4;
				// case 2 trying to move up and left
                if(two=='l'){
                    // s^1 case 1 desired move left is off the map
                    if(collumb==0)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row-1][collumb-1]==0){
                        board[row-1][collumb-1]=2;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row-1][collumb-1]==1 || board[row-1][collumb-1]==2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row-1][collumb-1]==-1 ||board[row-1][collumb-1]==-2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row-2)<=0 || (collumb-2)<=0)//////////////copy point//////////////////////////
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row-2][collumb-2]==0){
                            board[row-2][collumb-2]=2;
                            board[row][collumb]=0;
							board[row-1][collumb-1]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row-2][collumb-2]!=-1 && board[row-2][collumb-2]!=-2 && board[row-2][collumb-2]!=1 && board[row-2][collumb-2]!=2)
                            return -5;
                    }
                }// case 2 moves up and right or at least wants to
				if(two=='r'){
                    // s^1 case 1 desired move right is off the map
                    if(collumb==7)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row-1][collumb+1]==0){
                        board[row-1][collumb+1]=2;
                        board[row][collumb]=0;
                        return 1;
                    }/////////copy point ///////////////////////////////////////////
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row-1][collumb+1]==1 || board[row-1][collumb+1]==2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row-1][collumb+1]==-1 ||board[row-1][collumb+1]==-2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row-2)<=0 || (collumb+2)>=7)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row-2][collumb+2]==0){
                            board[row-2][collumb+2]=2;
                            board[row][collumb]=0;
                            board[row-1][collumb+1]=0;
							std::cout<<"JUMPED"<<std::endl;
							return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row-2][collumb+2]!=-1 && board[row-2][collumb+2]!=-2 && board[row-2][collumb+2]!=1 && board[row-2][collumb+2]!=2)
                            return -5;
                    }
                }
			}//moving down
            if(one=='b'){//////////////////copy point////////////////////////////////////////////////
                // case 1 trying to move down when allready at the bottom of the map
                if(row==7)
                    return -4;
                // case 2 trying to move down and left
                if(two=='l'){
                    // s^1 case 1 desired move left is off the map
                    if(collumb==0)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row+1][collumb-1]==0){
                        board[row+1][collumb-1]=2;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row+1][collumb-1]==1 || board[row+1][collumb-1]==2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row+1][collumb-1]==-1 ||board[row+1][collumb-1]==-2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row+2)>=7 || (collumb-2)<=0)///////////////////copy point//////////////////////
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row+2][collumb-2]==0){
                            board[row+2][collumb-2]=2;
                            board[row][collumb]=0;
							board[row+1][collumb-1]=0;
                            return 2;
							std::cout<<"JUMPED"<<std::endl;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row+2][collumb-2]!=-1 && board[row+2][collumb-2]!=-2 && board[row+2][collumb-2]!=1 && board[row+2][collumb-2]!=2)
                            return -5;
                    }
                }// case 2 moves down and right or at least wants to
                if(two=='r'){
                    // s^1 case 1 desired move right is off the map
                    if(collumb==7)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row+1][collumb+1]==0){
                        board[row+1][collumb+1]=2;
                        board[row][collumb]=0;
                        return 1;
                    }////////////////copy point///////////////
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row+1][collumb+1]==1 || board[row+1][collumb+1]==2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row+1][collumb+1]==-1 ||board[row+1][collumb+1]==-2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row+2)>=7 || (collumb+2)>=7)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row+2][collumb+2]==0){
                            board[row+2][collumb+2]=2;
							board[row+1][collumb+1]=0;
                            board[row][collumb]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row+2][collumb+2]!=-1 && board[row+2][collumb+2]!=-2 && board[row+2][collumb+2]!=1 && board[row+2][collumb+2]!=2)
                            return -5;
                    }
                }
            }
        }////////////////////////////////////copy point////////////////////////////////////
		if(check_king()!=1 && check_king()!=2)
	        return -6;
		return -20;
	}
	int p2_attack(int row, int collumb, char one, char two){
        //for p2 normal piece
      //  select_piece(row,collumb);
        if(current_piece_cord.first==-1){
            return -59000;
		}
        if(check_king()==-1){
            //checkers can't move backwards
			if(one=='b')
                return -5;
            if(one=='t'){
                // case 1 trying to move up when allready at the top of the map
                if(row==0)
                    return -40;
                // case 2 trying to move up and left
                if(two=='l'){
                    // s^1 case 1 desired move left is off the map
                    if(collumb==0)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row-1][collumb-1]==0){
                        board[row-1][collumb-1]=-1;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row-1][collumb-1]==-1 || board[row-1][collumb-1]==-2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row-1][collumb-1]==1 ||board[row-1][collumb-1]==2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row-2)<=0 || (collumb-2)<=0)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row-2][collumb-2]==0){
                            board[row-2][collumb-2]=-1;
                            board[row][collumb]=0;
							board[row-1][collumb-1]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row-2][collumb-2]!=-1 && board[row-2][collumb-2]!=-2 && board[row-2][collumb-2]!=1 && board[row-2][collumb-2]!=2)
                            return -51;
                    }
                }// case 2 moves up and right or at least wants to

                if(two=='r'){
                    // s^1 case 1 desired move right is off the map
                    if(collumb==7)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row-1][collumb+1]==0){
						board[row-1][collumb+1]=-1;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row-1][collumb+1]==-1 || board[row-1][collumb+1]==-2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row-1][collumb+1]==1 ||board[row-1][collumb+1]==2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row-2)<=0 || (collumb+2)>=7)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row-2][collumb+2]==0){
                            board[row-2][collumb+2]=-1;
                            board[row][collumb]=0;
							board[row-1][collumb+1]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row-2][collumb+2]!=-1 && board[row-2][collumb+2]!=-2 && board[row-2][collumb+2]!=1 && board[row-2][collumb+2]!=2)
                            return -50;
                    }
                }
            }
        }







        //for p2 king
        if(check_king()==-2){
            if(one=='t'){
                // case 1 trying to move up when allready at the top of the map
                if(row==0)
                    return -4;
                // case 2 trying to move up and left
                if(two=='l'){
                    // s^1 case 1 desired move left is off the map
                    if(collumb==0)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row-1][collumb-1]==0){
                        board[row-1][collumb-1]=-2;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row-1][collumb-1]==-1 || board[row-1][collumb-1]==-2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row-1][collumb-1]==1 ||board[row-1][collumb-1]==2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row-2)<=0 || (collumb-2)<=0)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row-2][collumb-2]==0){
                            board[row-2][collumb-2]=-2;
                            board[row][collumb]=0;
							board[row-1][collumb-1]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row-2][collumb-2]!=-1 && board[row-2][collumb-2]!=-2 && board[row-2][collumb-2]!=1 && board[row-2][collumb-2]!=2)
                            return -52;
                    }
                }// case 2 moves up and right or at least wants to
                if(two=='r'){
                    // s^1 case 1 desired move right is off the map
                    if(collumb==7)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row-1][collumb+1]==0){
                        board[row-1][collumb+1]=-2;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row-1][collumb+1]==-1 || board[row-1][collumb+1]==-2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row-1][collumb+1]==1 ||board[row-1][collumb+1]==2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row-2)<=0 || (collumb+2)>=7)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row-2][collumb+2]==0){
                            board[row-2][collumb+2]=-2;
                            board[row][collumb]=0;
							board[row-1][collumb+1]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row-2][collumb+2]!=-1 && board[row-2][collumb+2]!=-2 && board[row-2][collumb+2]!=1 && board[row-2][collumb+2]!=2)
                            return -54;
                    }
                }
            }//moving down
            if(one=='b'){
                // case 1 trying to move down when allready at the bottom of the map
                if(row==7)
                    return -4;
                // case 2 trying to move down and left
                if(two=='l'){
                    // s^1 case 1 desired move left is off the map
                    if(collumb==0)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row+1][collumb-1]==0){
                        board[row+1][collumb-1]=-2;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row+1][collumb-1]==-1 || board[row+1][collumb-1]==-2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row+1][collumb-1]==1 ||board[row+1][collumb-1]==2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row+2)>=7 || (collumb-2)<=0)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row+2][collumb-2]==0){
                            board[row+2][collumb-2]=-2;
                            board[row][collumb]=0;
							board[row+1][collumb-1]=0;
							std::cout<<"JUMPED"<<std::endl;
                            return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row+2][collumb-2]!=-1 && board[row+2][collumb-2]!=-2 && board[row+2][collumb-2]!=1 && board[row+2][collumb-2]!=2)
                            return -53;
                    }
                }// case 2 moves down and right or at least wants to
                if(two=='r'){
                    // s^1 case 1 desired move right is off the map
                    if(collumb==7)
                        return -3;
                    // s^1 case 2 there is nothing in desired move location and it can just move there
                    if(board[row+1][collumb+1]==0){
                        board[row+1][collumb+1]=-2;
                        board[row][collumb]=0;
                        return 1;
                    }
                    // s^1 case 2.5 there is a friendly in the desired location so you can't jump
                    if(board[row+1][collumb+1]==-1 || board[row+1][collumb+1]==-2){
                        return -6;
                    }
                    // s^1 case 3  there is an enemy in the desired location
                    if(board[row+1][collumb+1]==1 ||board[row+1][collumb+1]==2){ 
                        // s^2 case 1 the hop location is off the board
                        if( (row+2)>=7 || (collumb+2)>=7)
                            return -4;
                        //s^2 case 2 the space behind the piece is empty and hops there
                        if(board[row+2][collumb+2]==0){
                            board[row+2][collumb+2]=-2;
                            board[row][collumb]=0;
							board[row+1][collumb+1]=0;
                           	std::cout<<"JUMPED"<<std::endl;
							return 2;
                        }
                        //s^2 case 3 there is something in the hop desitnation so it can't hop or move
                        if(board[row+2][collumb+2]!=-1 && board[row+2][collumb+2]!=-2 && board[row+2][collumb+2]!=1 && board[row+2][collumb+2]!=2)
                            return -55;
                    }
                }
            }
        }
        if(check_king()!=-1 && check_king()!=-2)
            return -6;
		return -20;

	}
	int check_win(){
		/*forloop through board and check for pieces*/
		int p1_pieces=0;
		int p2_pieces=0;
		for(int i=0;i<8;i++){
			for(int j=0;j<8;j++){
				if(board[i][j]==-1 || board[i][j]==-2)
					p2_pieces++;
				if(board[i][j]==1 || board[i][j]==2)
                    p1_pieces++;
			}
		}
		if(p1_pieces==0)
			return 1;
		if(p2_pieces==0)
            return -1;
		return 0;
	}
	int getturn(){
		return turn;
	}
	int can_p2_jump(){
		check_jumps();
		return p2_jumps;
	}
	int can_p1_jump(){
		check_jumps();
        return p1_jumps;
    }
	void turn_complete(){
		if(turn==-1){
			turn=1;}
		else{
            turn=-1;}
	}
	void print(){
		for(int i=0;i<8;i++){
			for(int j=0;j<8;j++){
				std::cout<<board[i][j];
			}
			std::cout<<std::endl;
		}
		std::cout<<std::endl;
	}
	void printpiece(){
		std::cout<<current_piece_cord.first<<std::endl;
		std::cout<<current_piece_cord.second<<std::endl;
	}
private:
	int board[8][8]={{0}};
//turn starts with the bottom player aka player 2
	int turn=-1;
	int p1_jumps=0;
	int p2_jumps=0;
	int p1_pieces=12;
	int p2_pieces=12;
	Coord current_piece_cord/*=std::make_pair(-1,-1)*/;
};

#endif
