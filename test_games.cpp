/* Name: Evelyn Yeh
Course: EN.600.120
Date: Apr 28, 2016
Assignment Number: Final Project
Phone Number: 626-272-2182
JHED: eyeh8
Email: eyeh8@jhu.edu
*/

#include "battleship.hpp"
#include <iostream>
#include <cstdlib>
#include <cassert>

using namespace std;

void game1()
{
    Board player1_ships(10, 10);
    Board player2_ships(10, 10);
    BattleshipGame bsg;
    player1_ships = bsg.placeShip(player1_ships, 1, 3, 'v', 4, 1, "13v");
    player1_ships = bsg.placeShip(player1_ships, 4, 8, 'v', 3, 1, "48v");
    assert(bsg.getTestOutput().compare("INVALID MOVE") == 0);
    player1_ships = bsg.placeShip(player1_ships, 3, 6, 'h', 3, 1, "36h");
    player1_ships = bsg.placeShip(player1_ships, 0, 0, 'v', 2, 1, "00v");
    player1_ships = bsg.placeShip(player1_ships, 8, 0, 'h', 1, 1, "90h");
    player1_ships = bsg.placeShip(player1_ships, 5, 5, 'v', 0, 1, "55v");
    player2_ships = bsg.placeShip(player2_ships, 0, 9, 'v', 4, 2, "09v");
    assert(bsg.getTestOutput().compare("INVALID MOVE") == 0);
    player2_ships = bsg.placeShip(player2_ships, 2, 7, 'h', 4, 2, "27h");
    player2_ships = bsg.placeShip(player2_ships, 3, 1, 'v', 3, 2, "31v");
    player2_ships = bsg.placeShip(player2_ships, 9, 0, 'v', 2, 2, "90v");
    player2_ships = bsg.placeShip(player2_ships, 0, 2, 'h', 1, 2, "02h");
    player2_ships = bsg.placeShip(player2_ships, 9, 9, 'h', 0, 2, "99h");
    cout << "BEGIN" << endl;

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
//    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
//    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("INVALID MOVE") == 0);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
//    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
//    assert(bsg.getTestOutput().compare("SUNK SUBMARINE") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("INVALID MOVE") == 0);
    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("SUNK CRUISER") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("SUNK AIRCRAFT CARRIER") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("SUNK BATTLESHIP") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("INVALID MOVE") == 0);
    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("HIT") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("SUNK AIRCRAFT CARRIER") == 0);

    bsg.changeMoveSuccess(false);
    player1_ships = bsg.playGame(player1_ships, 2);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("MISS") == 0);

    bsg.changeMoveSuccess(false);
    player2_ships = bsg.playGame(player2_ships, 1);
    if(bsg.checkQuit())
        return;
    assert(bsg.getTestOutput().compare("SUNK DESTROYER") == 0);

    if(bsg.player1Dead())
    {
        cout << "PLAYER 2 WON" << endl;
        return;
    }
    if(bsg.player2Dead())
    {
        cout << "PLAYER 1 WON" << endl;
        return;
    }
}

int main()
{
    game1();
    return 0;
}
