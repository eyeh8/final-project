/* Name: Evelyn Yeh
Course number: 600.120
Date: Apr 22, 2016
Assignment number: HW5
Phone number: 626-272-2182
Blackboard login: eyeh8
Email: eyeh8@jhu.edu
*/
#ifndef BATTLESHIP_HPP_
#define BATTLESHIP_HPP_

#include <iostream>
#include <utility>
#include <array>
#include "game.hpp"
#include <cstdlib>
#include <algorithm>

typedef std::pair<int, int> Coord;

class Ship : public Game
{
public:
    Ship() : Game()
    {}

    Ship(int type, std::vector<Coord> coords, char o) : Game()
    {
        shipType = type;
        coordinates = coords;
        for(int i = 0; i <= type; i++)
            hitCoordinates.push_back(false);
        dir = o;
    }

    bool checkIfHit(int index) // return whether or not ship has sunk
    {
        return hitCoordinates.at(index);
    }

    void changeHit(int index)
    {
        hitCoordinates.at(index) = true;
    }

    bool checkIfSunk()
    {
        bool result = true;
        for(int i = 0; i < (int)hitCoordinates.size(); i++)
        {
            if(!hitCoordinates.at(i))
                result = false;
        }
        return result;
    }

    int getType() // returns type of ship, or number of spaces it takes up
    {
        return shipType;
    }

    std::vector<Coord> getCoord() // returns vector of coordinates of ship
    {
        return coordinates;
    }

    void incNumHits() // increases the number of hits on ship
    {
        numHits++;
    }

    int getNumHits()
    {
        return numHits;
    }

    char getDir()
    {
        return dir;
    }

    std::vector<Coord> updateCoord(std::vector<Coord> shipCoordinates, char o, int n)
    {
        for(Coord shipCoord : shipCoordinates)
        {
            if(o == 'u')
                shipCoord.second = shipCoord.second - n;
            else if(o == 'd')
                shipCoord.second = shipCoord.second + n;
            else if(o == 'l')
                shipCoord.first = shipCoord.first - n;
            else if(o == 'r')
                shipCoord.first = shipCoord.first + n;
        }
        return shipCoordinates;
    }

    void changeCoord(std::vector<Coord> coord)
    {
        coordinates = coord;
    }

private:
    int shipType;
    char dir;
    std::vector<Coord> coordinates;
    int numHits = 0;
    std::vector<bool> hitCoordinates;
};

class BattleshipGame : public Game
{
	std::vector<Ship> player1_ships;
    std::vector<Ship> player2_ships;
    Board player1_board = Board(10, 10);
    Board player2_board = Board(10, 10);
	int player1_numships;
	int player2_numships;
    bool moveSucceeded;
    bool mobileSucceeded;
    bool quit;
    std::string testOutput;
public:

	//constructor
	BattleshipGame() : Game(10, 10, 2)
	{
		player1_numships=5;
		player2_numships=5;
        moveSucceeded = false;
        mobileSucceeded = false;
        quit = false;
        testOutput = "";
    }

    std::string getTestOutput()
    {
        return testOutput;
    }

    bool getMoveSuccess() // return true if move was valid
    {
        return moveSucceeded;
    }

    void changeMoveSuccess(bool input) // mutator for main
    {
        moveSucceeded = input;
    }

    bool checkQuit() // accessor for main
    {
        return quit;
    }

    int checkPlayerShips(int player)
    {
        if(player == 1)
            return player1_numships;
        if(player == 2)
            return player2_numships;
        return 0;
    }

    bool player1Dead() // check if all player 1 ships have sunk
    {
        return player1_ships.empty();
    }

    bool player2Dead() // check if all player 2 ships have sunk
    {
        return player2_ships.empty();
    }

    bool coorEmpty(std::map<Coord, char> playerMap, Coord coordinate) // check if there is a ship on the coordinate
    {
        if(playerMap[coordinate] == 'x')
            return false;
        return true;
    }

    bool coorOnMap(Coord coordinate) // check if coordinate is valid on map
    {
        if(coordinate.first < 0 || coordinate.first > 9 || coordinate.second < 0 || coordinate.second > 9)
            return false;
        return true;
    }

	bool coorIsValid(Coord coordinate, int shipType, char direction) // check if entire ship will fit on map
	{
        if(!coorOnMap(coordinate))
            return false;
        if(direction == 'h' || direction == 'H')
        {
            return ((coordinate.first + shipType) < 10);
        }
        else if(direction == 'v' || direction == 'V')
        {
            return ((coordinate.second + shipType) < 10);
        }
        return false;
	}

    Board callInvalid(Board player_ships)
    {
        std::cout << "INVALID MOVE" << std::endl;
        moveSucceeded = false;
        testOutput = "INVALID MOVE";
        return player_ships;
    }

    std::vector<Coord> callInvalid(std::vector<Coord> player_ships)
    {
        std::cout << "INVALID MOVE" << std::endl;
        moveSucceeded = false;
        mobileSucceeded = false;
        testOutput = "INVALID MOVE";
        return player_ships;
    }

// place ship on map and push coordinates to ship
    Board placeShip(Board player_ships, int x, int y, char o, int type, int player, std::string input)
    {
        std::map<Coord, char> playerMap = player_ships.getMap();
        Coord coordinate = std::make_pair(x, y);
        std::vector<Coord> shipCoords;
        if(!coorIsValid(coordinate, type, o) || !coorEmpty(playerMap, coordinate) || input.length() != 3)
            return callInvalid(player_ships);
        for(int i = 0; i <= type; i++)
        {
            Coord newCoord = coordinate;
            if(o == 'h' || o == 'H')
            {
                newCoord = std::make_pair(coordinate.first + i, coordinate.second);
            }
            else if(o == 'v' || o == 'V')
            {
                newCoord = std::make_pair(coordinate.first, coordinate.second + i);
            }
            shipCoords.push_back(newCoord);
            playerMap[newCoord] = 'x';
        }
        Ship newShip(type, shipCoords, o);
        if(player == 1)
            player1_ships.push_back(newShip);
        if(player == 2)
            player2_ships.push_back(newShip);
        player_ships.changeMap(playerMap);
        moveSucceeded = true;
        return player_ships;
    }

// takes player input to place all of the ships
    Board placeAllShips(Board player_ships, int player)
    {
        std::string playerInput;
        int x;
        int y;
        char o;
        moveSucceeded = false;
        while(!moveSucceeded)
        {
            std::cout << "PLAYER " << player << " PLACE AIRCRAFT CARRIER:";
            std::cin >> playerInput;
            if(playerInput[0] == 'q')
            {
                quit = true;
                return player_ships;
            }
            x = playerInput[0] - '0';
            y = playerInput[1] - '0';
            o = playerInput[2];
            player_ships = placeShip(player_ships, x, y, o, 4, player, playerInput);
        }
        moveSucceeded = false;
        while(!moveSucceeded)
        {
            std::cout << "PLAYER " << player << " PLACE BATTLESHIP:";
            std::cin >> playerInput;
            if(playerInput[0] == 'q')
            {
                quit = true;
                return player_ships;
            }
            x = playerInput[0] - '0';
            y = playerInput[1] - '0';
            o = playerInput[2];
            player_ships = placeShip(player_ships, x, y, o, 3, player, playerInput);
        }
        moveSucceeded = false;
        while(!moveSucceeded)
        {
            std::cout << "PLAYER " << player << " PLACE CRUISER:";
            std::cin >> playerInput;
            if(playerInput[0] == 'q')
            {
                quit = true;
                return player_ships;
            }
            x = playerInput[0] - '0';
            y = playerInput[1] - '0';
            o = playerInput[2];
            player_ships = placeShip(player_ships, x, y, o, 2, player, playerInput);
        }
        moveSucceeded = false;
        while(!moveSucceeded)
        {
            std::cout << "PLAYER " << player << " PLACE SUBMARINE:";
            std::cin >> playerInput;
            if(playerInput[0] == 'q')
            {
                quit = true;
                return player_ships;
            }
            x = playerInput[0] - '0';
            y = playerInput[1] - '0';
            o = playerInput[2];
            player_ships = placeShip(player_ships, x, y, o, 1, player, playerInput);
        }
        moveSucceeded = false;
        while(!moveSucceeded)
        {
            std::cout << "PLAYER " << player << " PLACE DESTROYER:";
            std::cin >> playerInput;
            if(playerInput[0] == 'q')
            {
                quit = true;
                return player_ships;
            }
            x = playerInput[0] - '0';
            y = playerInput[1] - '0';
            o = playerInput[2];
            player_ships = placeShip(player_ships, x, y, o, 0, player, playerInput);
        }
        if(player == 1)
            player1_board = player_ships;
        if(player == 2)
            player2_board = player_ships;
        moveSucceeded = true;
        return player_ships;
    }

// returns true if coordinates are equal
    bool coorEqual(Coord coor1, Coord coor2)
    {
        if((coor1.first == coor2.first) && (coor1.second == coor2.second))
            return true;
        return false;
    }

// increase number of hits on ship, called when one is hit
    std::vector<Ship> hitShip(std::vector<Ship> player_ships, Coord coordinate)
    {
        for(int a = 0; a < (int)player_ships.size(); a++)
        {
            Ship tempShip = player_ships.at(a);
            std::vector<Coord> shipCoordinates = tempShip.getCoord();
            for(int i = 0; i < (int)shipCoordinates.size(); i++)
            {
                Coord shipCoord = shipCoordinates.at(i);
                if(coorEqual(coordinate, shipCoord) && !tempShip.checkIfHit(i))
                {
                    tempShip.incNumHits();
                    tempShip.changeHit(i);
                    player_ships.erase(player_ships.begin()+a);
                    player_ships.push_back(tempShip);
//                    std::cout << player_ships.at((int)player_ships.size()-1).getNumHits() << std::endl;
                    break;
                }
            }
        }
        return player_ships;
    }

bool outputSunk(int type)
{
    if(type == 4)
    {
        std::cout << "SUNK AIRCRAFT CARRIER" << std::endl;
        testOutput = "SUNK AIRCRAFT CARRIER";
        return true;
    }
    else if(type == 3)
    {
        std::cout << "SUNK BATTLESHIP" << std::endl;
        testOutput = "SUNK BATTLESHIP";
        return true;
    }
    else if(type == 2)
    {
        std::cout << "SUNK CRUISER" << std::endl;
        testOutput = "SUNK CRUISER";
        return true;
    }
    else if(type == 1)
    {
        std::cout << "SUNK SUBMARINE" << std::endl;
        testOutput = "SUNK SUBMARINE";
        return true;
    }
    else if(type == 0)
    {
        std::cout << "SUNK DESTROYER" << std::endl;
        testOutput = "SUNK DESTROYER";
        return true;
    }
    return false;
}

// makes changes after the hit, checks if the ship has sunk and erases it accordingly
    std::vector<Ship> postShip(std::vector<Ship> player_ships, int player)
    {
        bool sunk;
        for(auto it = player_ships.begin(); it != player_ships.end(); it++)
        {
            Ship currentShip = *it;
            if(currentShip.checkIfSunk())
            {
                std::vector<Coord> shipCoordinates = currentShip.getCoord();
                for(auto shipIt = shipCoordinates.begin(); shipIt != shipCoordinates.end(); shipIt++)
                {
                    Coord shipCoord = *shipIt;
                    if(player == 1)
                    {
                        std::map<Coord, char> player1_map = player1_board.getMap();
                        player1_map[shipCoord] = '-';
                        player1_board.changeMap(player1_map);
                    }
                    if(player == 2)
                    {
                        std::map<Coord, char> player2_map = player2_board.getMap();
                        player2_map[shipCoord] = '-';
                        player2_board.changeMap(player2_map);
                    }
                }
                sunk = outputSunk(currentShip.getType());
                player_ships.erase(it);
                if(player == 1)
                    player1_numships--;
                if(player == 2)
                    player2_numships--;
                break;
            }
        }
        if(!sunk)
        {
            std::cout << "HIT" << std::endl;
            testOutput = "HIT";
        }
        return player_ships;
    }

// main method for playing the game
// takes player input and judges whether it is a hit or miss
	Board playGame(Board player_ships, int player)
	{
        std::string playerInput;
        int x;
        int y;
        std::cout << "PLAYER " << player << ":";
        std::cin >> playerInput;
        if(playerInput[0] == 'q')
        {
            quit = true;
            return player_ships;
        }
        x = playerInput[0] - '0';
        y = playerInput[1] - '0';
        Coord tempCoord = std::make_pair(x, y);
        if(!coorOnMap(tempCoord) || playerInput.length() != 2)
            return callInvalid(player_ships);
        if(player == 1)
        {
            auto player2_map = player2_board.getMap();
            if(player2_map[tempCoord] == '-')
            {
                std::cout << "MISS" << std::endl;
                testOutput = "MISS";
                moveSucceeded = true;
                return player2_board;
            }
            if(player2_map[tempCoord] == 'x')
            {
                player2_ships = hitShip(player2_ships, tempCoord);
                player2_ships = postShip(player2_ships, 2);
                testOutput = "HIT";
                moveSucceeded = true;
                return player2_board;
            }
        }
        else if(player == 2)
        {
            auto player1_map = player1_board.getMap();
            if(player1_map[tempCoord] == '-')
            {
                std::cout << "MISS" << std::endl;
                testOutput = "MISS";
                moveSucceeded = true;
                return player1_board;
            }
            if(player1_map[tempCoord] == 'x')
            {
                player1_ships = hitShip(player1_ships, tempCoord);
                player1_ships = postShip(player1_ships, 1);
                testOutput = "HIT";
                moveSucceeded = true;
                return player1_board;
            }
        }
        return player_ships;
    }

    std::vector<Ship> changeShip(Ship newShip, std::vector<Ship> player_ships)
    {
        for(int i = 0; i < (int) player_ships.size(); i++)
        {
            Ship tempShip = player_ships.at(i);
            if(tempShip.getType() == newShip.getType())
            {
                player_ships.at(i) = newShip;
                break;
            }
        }
        return player_ships;
    }

    std::vector<Coord> moveCoord(Ship chosenShip, char d, int n, int player)
    {
        std::vector<Coord> shipCoordinates = chosenShip.getCoord();
        std::map<Coord, char> player_map;
        if(player == 1)
            player_map = player1_board.getMap();
        if(player == 2)
            player_map = player2_board.getMap();
        for(int b = 0; b < (int)shipCoordinates.size(); b++)
        {
            Coord shipCoord = shipCoordinates.at(b);
            player_map[shipCoord] = '-';
        }
        for(int b = 0; b < (int)shipCoordinates.size(); b++)
        {
            Coord shipCoord = shipCoordinates.at(b);
            if(player == 1)
            {
                if(d == 'u')
                {
                    if((shipCoord.second - n) < 0 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
                else if(d == 'd')
                {
                    if((shipCoord.second + n) > 9 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
                else if(d == 'l')
                {
                    if((shipCoord.first - n) < 0 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
                else if(d == 'r')
                {
                    if((shipCoord.first + n) > 9 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
            }
            if(player == 2)
            {
                if(d == 'u')
                {
                    if((shipCoord.second - n) < 0 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
                else if(d == 'd')
                {
                    if((shipCoord.second + n) > 9 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
                else if(d == 'l')
                {
                    if((shipCoord.first - n) < 0 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
                else if(d == 'r')
                {
                    if((shipCoord.first + n) > 9 || !coorEmpty(player_map, shipCoord))
                        return callInvalid(shipCoordinates);
                }
            }
        }
        std::vector<Coord> newCoord;
        for(int a = 0; a < (int)shipCoordinates.size(); a++)
        {
            Coord shipCoord = shipCoordinates.at(a);
            if(d == 'u')
            {
                Coord tempCoord = std::make_pair(shipCoord.first, shipCoord.second - n);
                newCoord.push_back(tempCoord);
            }
            else if(d == 'd')
            {
                Coord tempCoord = std::make_pair(shipCoord.first, shipCoord.second + n);
                newCoord.push_back(tempCoord);
            }
            else if(d == 'l')
            {
                Coord tempCoord = std::make_pair(shipCoord.first - n, shipCoord.second);
                newCoord.push_back(tempCoord);
            }
            else if(d == 'r')
            {
                Coord tempCoord = std::make_pair(shipCoord.first + n, shipCoord.second);
                newCoord.push_back(tempCoord);
            }
        }
        chosenShip.changeCoord(newCoord);
//        chosenShip.changeCoord(chosenShip.updateCoord(shipCoordinates, d, n));
        shipCoordinates = chosenShip.getCoord();
        for(int b = 0; b < (int)shipCoordinates.size(); b++)
        {
            Coord shipCoord = shipCoordinates.at(b);
            player_map[shipCoord] = 'x';
        }
        if(player == 1)
        {
            player1_ships = changeShip(chosenShip, player1_ships);
            player1_board.changeMap(player_map);
        }
        if(player == 2)
        {
            player2_ships = changeShip(chosenShip, player2_ships);
            player2_board.changeMap(player_map);
        }
        mobileSucceeded = true;
        return shipCoordinates;
    }

	Board playMobileGame(Board player_ships, int player)
	{
        std::string playerInput;
        int x;
        int y;
        char d;
        int n;
        std::cout << "PLAYER " << player << ":";
        std::cin >> playerInput;
        if(playerInput[0] == 'q')
        {
            quit = true;
            return player_ships;
        }
        x = playerInput[0] - '0';
        y = playerInput[1] - '0';
        Coord tempCoord = std::make_pair(x, y);
        if(!coorOnMap(tempCoord))
            return callInvalid(player_ships);
        if(playerInput.length() == 2)
        {
            if(player == 1)
            {
                auto player2_map = player2_board.getMap();
                if(player2_map[tempCoord] == '-')
                {
                    std::cout << "MISS" << std::endl;
                    testOutput = "MISS";
                    moveSucceeded = true;
                    return player2_board;
                }
                if(player2_map[tempCoord] == 'x')
                {
                    player2_ships = hitShip(player2_ships, tempCoord);
                    player2_ships = postShip(player2_ships, 2);
                    moveSucceeded = true;
                    return player2_board;
                }
            }
            else if(player == 2)
            {
                auto player1_map = player1_board.getMap();
                if(player1_map[tempCoord] == '-')
                {
                    std::cout << "MISS" << std::endl;
                    testOutput = "MISS";
                    moveSucceeded = true;
                    return player1_board;
                }
                if(player1_map[tempCoord] == 'x')
                {
                    player1_ships = hitShip(player1_ships, tempCoord);
                    player1_ships = postShip(player1_ships, 1);
                    moveSucceeded = true;
                    return player1_board;
                }
            }
        }
        else if(playerInput.length() == 4)
        {
            d = playerInput[2];
            n = playerInput[3] - '0';
            if(n == 0 || n > 3)
                return callInvalid(player_ships);
            if(player == 1)
            {
                auto player1_map = player1_board.getMap();
                if(coorEmpty(player1_map, tempCoord))
                    return callInvalid(player_ships);
                Ship chosenShip;
                for(int i = 0; i < (int)player1_ships.size(); i++)
                {
                    Ship tempShip = player1_ships.at(i);
                    std::vector<Coord> shipCoordinates = tempShip.getCoord();
                    for(int a = 0; a < (int)shipCoordinates.size(); a++)
                    {
                        Coord shipCoord = shipCoordinates.at(a);
                        if(coorEqual(tempCoord, shipCoord))
                        {
                            chosenShip = player1_ships.at(i);
                            break;
                        }
                    }
                }
                if(d == 'u' || d == 'd')
                {
                    if(chosenShip.getDir() != 'v')
                        return callInvalid(player_ships);
                    chosenShip.changeCoord(moveCoord(chosenShip, d, n, player));
                    if(mobileSucceeded)
                        moveSucceeded = true;
                    return player1_board;
                }
                else if(d == 'l' || d == 'r')
                {
                    if(chosenShip.getDir() != 'h')
                        return callInvalid(player_ships);
                    chosenShip.changeCoord(moveCoord(chosenShip, d, n, player));
                    if(mobileSucceeded)
                        moveSucceeded = true;
                    return player1_board;
                }
                else
                    return callInvalid(player_ships);
            }
            else if(player == 2)
            {
                auto player2_map = player2_board.getMap();
                if(coorEmpty(player2_map, tempCoord))
                    return callInvalid(player_ships);
                Ship chosenShip;
                for(int i = 0; i < (int)player2_ships.size(); i++)
                {
                    Ship tempShip = player2_ships.at(i);
                    std::vector<Coord> shipCoordinates = tempShip.getCoord();
                    for(int a = 0; a < (int)shipCoordinates.size(); a++)
                    {
                        Coord shipCoord = shipCoordinates.at(a);
                        if(coorEqual(tempCoord, shipCoord))
                        {
                            chosenShip = player2_ships.at(i);
                            break;
                        }
                    }
                }
                if(d == 'u' || d == 'd')
                {
                    if(chosenShip.getDir() != 'v')
                        return callInvalid(player_ships);
                    chosenShip.changeCoord(moveCoord(chosenShip, d, n, player));
                    if(mobileSucceeded)
                        moveSucceeded = true;
                    return player2_board;
                }
                else if(d == 'l' || d == 'r')
                {
                    if(chosenShip.getDir() != 'h')
                        return callInvalid(player_ships);
                    chosenShip.changeCoord(moveCoord(chosenShip, d, n, player));
                    if(mobileSucceeded)
                        moveSucceeded = true;
                    return player2_board;
                }
                else
                    return callInvalid(player_ships);
            }
        }
        else
            return callInvalid(player_ships);
        return player_ships;
    }
};

#endif /* BATTLESHIP_HPP_ */

