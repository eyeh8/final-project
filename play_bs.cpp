#include "battleship.hpp"
#include <iostream>
#include <cassert>
#include <array>

typedef std::pair<int, int> Coord;

using namespace std;

/*void game1()
{

  array<Coord, 5> player1_ships = {{
      make_pair(2, 2),
      make_pair(4, 4),
      make_pair(5, 5),
      make_pair(6, 6),
      make_pair(8, 8)
    }};
  array<Coord, 5> player2_ships = {{
      make_pair(2, 2),
      make_pair(4, 4),
      make_pair(5, 5),
      make_pair(6, 6),
      make_pair(8, 8)
    }};
  BattleshipGame bsg(player1_ships, player2_ships);

  GameResult result = bsg.attack_square(make_pair(2, 2)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);


  result = bsg.attack_square(make_pair(2, 2)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  // oops, already an X on this square
  result = bsg.attack_square(make_pair(2, 2)); // P1 invalid move
  assert(result == RESULT_INVALID);

  // note: an invalid move doesn't count as a turn; still player 1's turn

  result = bsg.attack_square(make_pair(4, 4)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(4, 4)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(5, 5)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(5, 5)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(6, 6)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(6, 6)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(8, 8)); // P1 HIT
  assert(result == RESULT_PLAYER1_WINS);

  cout << "PASSED game 1" << endl;
}


void game2()
{
  array<Coord, 5> player1_ships = {{
      make_pair(2, 2),
      make_pair(4, 4),
      make_pair(5, 5),
      make_pair(6, 6),
      make_pair(8, 8)
    }};
  array<Coord, 5> player2_ships = {{
      make_pair(2, 2),
      make_pair(4, 4),
      make_pair(5, 5),
      make_pair(6, 6),
      make_pair(8, 8)
    }};
  BattleshipGame bsg(player1_ships, player2_ships);

  GameResult result = bsg.attack_square(make_pair(50, 2)); // First coordinate out of range
  assert(result == RESULT_INVALID);

  result = bsg.attack_square(make_pair(2, 2)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(2, 2)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(2, 43)); // Second coordinate out of range
  assert(result == RESULT_INVALID);

  result = bsg.attack_square(make_pair(40, 40)); //Both coordinates out of range
  assert(result == RESULT_INVALID);

  result = bsg.attack_square(make_pair(4, 4)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(4, 4)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(5, 5)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(5, 5)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(6, 6)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(6, 6)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(3, 7)); // P1 should miss
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(8, 8)); // P2 HIT and wins
  assert(result == RESULT_PLAYER2_WINS);


  cout << "PASSED game 2" << endl;
}

void game3()
{
  array<Coord, 5> player1_ships = {{
      make_pair(2, 2),
      make_pair(4, 4),
      make_pair(5, 5),
      make_pair(6, 6),
      make_pair(8, 8)
    }};
  array<Coord, 5> player2_ships = {{
      make_pair(1, 3),
      make_pair(3, 4),
      make_pair(4, 5),
      make_pair(7, 6),
      make_pair(8, 9)
    }};
  BattleshipGame bsg(player1_ships, player2_ships);

  GameResult result = bsg.attack_square(make_pair(2, 2)); // P1 should miss
  assert(result == RESULT_KEEP_PLAYING);


  result = bsg.attack_square(make_pair(2, 2)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);


  result = bsg.attack_square(make_pair(2, 2)); // P1 invalid move
  assert(result == RESULT_INVALID);


  result = bsg.attack_square(make_pair(1, 3)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(4, 4)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(5, 5)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(5, 5)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(6, 6)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(6, 6)); // P2 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(8, 8)); // P1 HIT
  assert(result == RESULT_KEEP_PLAYING);

  result = bsg.attack_square(make_pair(8, 8)); // P2 HIT and wins
  assert(result == RESULT_PLAYER2_WINS);

  cout << "PASSED game 3" << endl;
}

void game4()
{
	cout<< "game4: succesfully checked for wrong inputs: ";
	array<Coord, 5> player1_ships = {{
      make_pair(100, 2),
      make_pair(20, 4),
      make_pair(5, 5),
      make_pair(6, 6),
      make_pair(8, 8)
    }};
	array<Coord, 5> player2_ships = {{
      make_pair(2, 2),
      make_pair(2, 2),
      make_pair(2, 2),
      make_pair(2, 2),
      make_pair(8, 8)
	}};

	BattleshipGame bsg(player1_ships, player2_ships);

}*/


int main(void)
{
/*  game1();
  game2();
  game3();
  game4();*/
  return 0;
}

