1. TEAM

Evelyn Yeh - eyeh8
Dimitri Herr - dherr6

2. DESIGN

For battleship, we used both vectors and maps in our classes. We used maps to determine whether or not there were ships already existing on the coordinates,
with coordinates as the keys and '-' or 'x' as the values. We had vectors of the class Ship that had their own vectors of Coord. In placing the ships on
the board, we would change the appropriate coordinates on the map to 'x', push the coordinates to the Ship's vector, and then push the Ships to 
BattleshipGame's vector of Ships. When playing the game, we would loop through these vectors to find the corresponding ships and alter them, both in moving
and deleting them when they are sunk.

for checkers we used a 2d array of ints for the board having diffrent integer values represent diffrent pieces. We simply checked spots on the board
 relative to the inputed position for movement. We also checked avaliable jumps by looping through the board. The reason we did not use similar things in our battleship and checkers was that the objects could be represented more simply in checkers. 

3. REQUIRED LANGUAGE FEATURES

We used vectors and maps for battleship as described above (container classes). We had a couple of classes: Game, Board, BattleshipGame, and Ship, the
latter two which inherit from Game with their respective functions and members. We used polymorphism with the callInvalid function, which we decided
to use to return either a vector or Board as needed. We overloaded the << operator to be able to print out the maps of the ships.

4. COMPLETENESS

As far as we tested, battleship and mobile battleship are complete and error-free. Although we wrote a testing file for battleship, it does not
work properly for some reason. If you use the games executable directly, the games should run correctly.

checkers works

5. SPECIAL INSTRUCTIONS

Test using games exectuable. Unfortunately, we did not have time to fix the actual tester.

Other notes: We were quite pressed for time since our group consists of only 2 people rather than 3. 
