#include "checkers.hpp"
#include "battleship.hpp"
#include <iostream>
#include <string>
#include <array>

using namespace std;

int main()
{
    cout << "CHOOSE A GAME:";
    int gameOption;
    string playerInput;
    cin >> gameOption;
    if(gameOption == 1)
    {
        Board player1_ships(10, 10);
        Board player2_ships(10, 10);
        BattleshipGame bsg;
// placing the ships on the boards
        player1_ships = bsg.placeAllShips(player1_ships, 1);
        if(bsg.checkQuit())
            return 0;
        player2_ships = bsg.placeAllShips(player2_ships, 2);
        if(bsg.checkQuit())
            return 0;
        cout << "BEGIN" << endl;
        while(1)
        {
            if(bsg.player1Dead())
            {
                cout << "PLAYER 2 WON" << endl;
                return 0;
            }
            if(bsg.player2Dead())
            {
                cout << "PLAYER 1 WON" << endl;
                return 0;
            }
            bsg.changeMoveSuccess(false);
            while(!bsg.getMoveSuccess())
            {
                player2_ships = bsg.playGame(player2_ships, 1);
                if(bsg.checkQuit())
                    return 0;
            }
            if(bsg.player2Dead())
            {
                cout << "PLAYER 1 WON" << endl;
                return 0;
            }
            bsg.changeMoveSuccess(false);
            while(!bsg.getMoveSuccess())
            {
                player1_ships = bsg.playGame(player1_ships, 2);
                if(bsg.checkQuit())
                    return 0;
            }
        }
    }
    if(gameOption == 2)
    {
        Board player1_ships(10, 10);
        Board player2_ships(10, 10);
        BattleshipGame bsg;
// placing the ships on the boards
        player1_ships = bsg.placeAllShips(player1_ships, 1);
        if(bsg.checkQuit())
            return 0;
        player2_ships = bsg.placeAllShips(player2_ships, 2);
        if(bsg.checkQuit())
            return 0;
        cout << player1_ships << endl;
        cout << player2_ships << endl;
        while(1)
        {
            if(bsg.player1Dead())
            {
                cout << "PLAYER 2 WON" << endl;
                return 0;
            }
            if(bsg.player2Dead())
            {
                cout << "PLAYER 1 WON" << endl;
                return 0;
            }
            bsg.changeMoveSuccess(false);
            while(!bsg.getMoveSuccess())
            {
                player2_ships = bsg.playMobileGame(player2_ships, 1);
                if(bsg.checkQuit())
                    return 0;
                cout << player1_ships << endl;
            }
            bsg.changeMoveSuccess(false);
            while(!bsg.getMoveSuccess())
            {
                player1_ships = bsg.playMobileGame(player1_ships, 2);
                if(bsg.checkQuit())
                    return 0;
                cout << player2_ships << endl;
            }
        }
    }
    if(gameOption == 3)
    {
        cout << "checkers" << endl;
    std::string playerInput;
    int piece=0;
    int tmark=0;
    int marker=0;
    int xshift=0;
    int yshift=0;
//  int xcord;
//  int ycord;
    int p2_attack_result;
    int p1_attack_result;
    checkers cg;
    cg.fillboard();
    cg.print();
    //initial case
    while(cg.check_win()==0){
        //case 1 player 2 turn
        if(cg.getturn()==-1){
            std::cout<<"please input a command it is player 1 the bottom players turn"<<std::endl;
            std::cin >> playerInput;
            //35tl
            // s1 case 1 invalid input because not on the board
            while( (int)(playerInput[0]-'0')>7 || (int)(playerInput[0]-'0')<0 || (int)(playerInput[1]-'0')>7 || (int)(playerInput[1]-'0')<0){
                std::cout<<"invalid input please try again not on board "<<std::endl;
                std::cin >> playerInput;
            }
            // s1 case 2 invalid input for selecting a piece your allowed to on your turn or a null space
            piece=cg.select_piece((int)(playerInput[1]-'0'),(int)(playerInput[0]-'0'));
            while(piece==0){
                std::cout<<"invalid input please try again not your piece"<<std::endl;
                std::cin >> playerInput;
                piece=cg.select_piece((int)(playerInput[1]-'0'),(int)(playerInput[0]-'0'));
            }
            // s1 case 3 last two inputs are incorect
            while((playerInput[2]!='t' &&playerInput[2]!='b')&&(playerInput[3]!='r' &&playerInput[3]!='l')){
                std::cout<<"invalid input please try again invalid direction"<<std::endl;
                std::cin >> playerInput;
                piece=cg.select_piece(playerInput[1],playerInput[0]);
            }
            // s1 case 4 piece selection now do move
            // s2 case 1 can jump and therefore must jump
            if( ((cg.can_p2_jump()==1&&marker==0) || (cg.check_jumps((int)(playerInput[1]-'0'),(int)(playerInput[0]-'0'))==-1&&marker>0)) ){
                p2_attack_result=cg.p2_attack((int)(playerInput[1]-'0'), (int)(playerInput[0]-'0'), playerInput[2], playerInput[3]);
                if(p2_attack_result==1){
                    // s3 case 1 succesfuly hop turn continues because more hops avaliable
                    cg.make_king();
                    if(playerInput[2]=='t')
                        yshift=-2;
                    if(playerInput[2]=='b')
                        yshift=2;
                    if(playerInput[3]=='l')
                        xshift=-2;
                    if(playerInput[3]=='r')
                        xshift=2;
                    if(cg.check_jumps((int)(playerInput[1]-'0')+yshift,(int)(playerInput[0]-'0')+xshift)==1){
                        std::cout<<"JUMPED"<<std::endl;
                        std::cout<<"another jump is avaliable with the same piece"<<std::endl;
                    //  xcord=playerInput[0]+xshift;
                    //  ycord=playerInput[1]+yshift;
                        marker++;
                    }// s3 case 2 succesfuly hop turn ends because no more hops avaliable
                    else{
                        std::cout<<"JUMPED"<<std::endl;
                        tmark++;
                        marker=0;
                    //  xcord=0;
                    //  ycord=0;
                    }
                }
            }//test
            // s2 case 2 can't jump and must move
            if(cg.can_p2_jump()==0 && marker==0){
                p2_attack_result=cg.p2_attack((int)(playerInput[1]-'0'), (int)(playerInput[0]-'0'), playerInput[2], playerInput[3]);
                if(p2_attack_result==1){
                    //piece moved end turn
                    tmark++;
                    cg.make_king();
                }
            }
        }
        //case 2 player 1 turn
        if(cg.getturn()==1){
            std::cout<<"please input a command it is player 2 the top players turn"<<std::endl;
            std::cin >> playerInput;
            //35tl
            // s1 case 1 invalid input because not on the board
            while((int)(playerInput[0]-'0')>7 || (int)(playerInput[0]-'0')<0 || (int)(playerInput[1]-'0')>7 || (int)(playerInput[1]-'0')<0){
                std::cout<<"invalid input please try again not on board "<<std::endl;
                std::cin >> playerInput;
            }
            // s1 case 2 invalid input for selecting a piece your allowed to on your turn or a null space
            piece=cg.select_piece((int)(playerInput[1]-'0'),(int)(playerInput[0]-'0'));
            while(piece==0){
                std::cout<<"invalid input please try again not your piece"<<std::endl;
                std::cin >> playerInput;
                piece=cg.select_piece((int)(playerInput[1]-'0'),(int)(playerInput[0]-'0'));
            }
            // s1 case 3 last two inputs are incorect
            while( (playerInput[2]!='t' &&playerInput[2]!='b')&&(playerInput[3]!='r' &&playerInput[3]!='l') ){
                std::cout<<"invalid input please try again invalid direction"<<std::endl;
                std::cin >> playerInput;
                piece=cg.select_piece((int)(playerInput[1]-'0'),(int)(playerInput[0]-'0'));
            }
            // s1 case 4 piece selection now do move
            // s2 case 1 can jump and therefore must jump
            if( ((cg.can_p1_jump()==1&&marker==0) || (cg.check_jumps((int)(playerInput[1]-'0'),(int)(playerInput[0]-'0'))==1&&marker>0)) ){
                p1_attack_result=cg.p1_attack((int)(playerInput[1]-'0'), (int)(playerInput[0]-'0'), playerInput[2], playerInput[3]);
                if(p1_attack_result==1){
                    // s3 case 1 succesfuly hop turn continues because more hops avaliable
                    cg.make_king();
                    if(playerInput[2]=='t')
                        yshift=-2;
                    if(playerInput[2]=='b')
                        yshift=2;
                    if(playerInput[3]=='l')
                        xshift=-2;
                    if(playerInput[3]=='r')
                        xshift=2;
                    if(cg.check_jumps((int)(playerInput[1]-'0')+yshift,(int)(playerInput[0]-'0')+xshift)==1){
                        std::cout<<"JUMPED"<<std::endl;
                        std::cout<<"another jump is avaliable with the same piece"<<std::endl;
                      //  xcord=(int)(playerInput[0]-'0')+xshift;
                      //  ycord=(int)(playerInput[1]-'0')+yshift;
                        marker++;
                    }// s3 case 2 succesfuly hop turn ends because no more hops avaliable
                    else{
                        std::cout<<"JUMPED"<<std::endl;
                        tmark++;
                        marker=0;
                //      xcord=0;
                //        ycord=0;
                    }
                }
            }
            // s2 case 2 can't jump and must move
            if(cg.can_p1_jump()==0 && marker==0){
                p1_attack_result=cg.p1_attack((int)(playerInput[1]-'0'), (int)(playerInput[0]-'0'), playerInput[2], playerInput[3]);
                if(p1_attack_result==1){
                    //piece moved end turn
                    cg.make_king();
                    tmark++;
                }
            }
	}
	if(tmark>0){
            cg.turn_complete();
        }
	cg.print();
    }
    if(cg.check_win()==1)
        std::cout<<"player 1 wins"<<std::endl;
    if(cg.check_win()==-1)
        std::cout<<"player 2 wins"<<std::endl;

    }
    return 0;
}
