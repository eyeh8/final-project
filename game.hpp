#ifndef GAME_H
#define GAME_H

/*
 * Use this file as a *starting point*.  You may add more classes and other
 * definitions here.
 *
 * We have suggested a couple ideas (Board and Game) for base classes you can
 * use in your design.
 *
 * Implement derived types for the games in "battleship.h" and "tictactoe.h"
 *
 * You must use "enum GameResult" and "Coord" as defined here, and you must
 * implement derived types called BattleshipGame and TicTacToeGame, each with
 * a public attack_square member function, as you can see from reading
 * play_bs.cpp and play_ttt.cpp.
 */

#include <utility>
#include <map>
#include <vector>
#include <iostream>



enum GameResult {
    RESULT_KEEP_PLAYING, // turn was valid and game is not over yet
    RESULT_INVALID,      // turn was invalid; e.g. attacked square
                         // was attacked previously
    RESULT_STALEMATE,    // game over, neither player wins
    RESULT_PLAYER1_WINS, // game over, player 1 wins
    RESULT_PLAYER2_WINS  // game over, player 2 wins
};

typedef std::pair<int, int> Coord;

class Board {
    std::map<Coord, char> gameBoard;
public:
    Board() {}
    Board(int x, int y)
    {
    	for(int i = 0; i < x; i++)
    	{
    		for (int k = 0; k < y; k++)
    		{
    			gameBoard[std::make_pair(i,k)] = '-';
    		}
    	}
    }
    std::map<Coord, char> getMap()
	{
    	return gameBoard;
	}
    void addAt(Coord coor, char c)
    {
    	gameBoard[coor] = c;
    }
    char get(Coord coor)
    {
    	return gameBoard[coor];
    }
    void changeMap(std::map<Coord, char> newMap)
    {
        gameBoard = newMap;
    }
    friend std::ostream& operator<<(std::ostream& out, Board b)
    {
        for(int y = 0; y < 10; y++)
        {
            for(int x = 0; x < 10; x++)
            {
                auto map = b.getMap();
                out << map[std::make_pair(x, y)] << " ";
            }
            out << std::endl;
        }
        return out;
    }
};

class Game {
protected:
    int player;
    std::vector<Board> b;
public:
    Game()
	{
	}
    Game(int x, int y, int numBoards)
    {
    	for ( int i = 0; i < numBoards; i++ )
    	{
    		b.push_back(Board(x, y));
    	}
    	player = 1;
    }
};

#endif
